# Installation #

If it`s necessary install this packages
```
#!bash
sudo apt-get install libpq-dev python-dev postgresql postgresql-contrib
```
Install virtualenv
```
#!bash
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install virtualenv
```

## Deploying project ##

**Variant 1**. With new database
```
#!bash
sudo su - postgres
createdb gebi
exit
sudo service postgresql restart

git clone git@bitbucket.org:barbossa/gebi-project.git
cd gebi-project
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver 0.0.0.0:8000
```
**Variant 2**. With my database. I recommend this variant.
```
#!bash
git clone git@bitbucket.org:barbossa/gebi-project.git
cd gebi-project
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt

sudo su - postgres
createdb gebi
psql gebi < gebi.sql
exit
sudo service postgresql restart

python manage.py runserver 0.0.0.0:8000
```

Admin user:
username - admin
password - 1