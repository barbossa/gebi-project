from django.conf.urls import include, url
from django.contrib import admin
from blog.views import index, signup, login, logout

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^signup', signup, name='signup'),
    url(r'^login', login, name='login'),
    url(r'^logout', logout, name='logout'),
    url(r'^admin/', include(admin.site.urls)),
]
